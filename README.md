# React GraphQL Exercise

For this exercise you will be creating a single page React application that allows a user to search [The SpaceX GraphQL server](https://api.spacex.land/graphql/) for upcoming and past SpaceX launches.

## User Story

As a user I want to be able to search SpaceX launches to find ones I am interested in 
    
### Acceptance Criteria:
- Users should be able to search by:
	- Mission name
	- Rocket name
	- Launch year
- Results should include: 
	- Mission name
	- Rocket name
	- Launch date 
	- Video link

### Show your work

Please provide a git repository link so we can follow along. Once you feel your finished with the exercise let us know. 
We should be able to pull down your repo, follow readme instructions and successfully run your app. 
We will expect to be able to do all of the actions specified in the acceptance criteria.

